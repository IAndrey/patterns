package behavioral;

public class Visitor {
    public static void main(String[] args) {
        Car engine = new Engine();
        engine.accept(new CarVisitor());
    }
}

interface IVisitor {
    void visitEngine();
    void visitWhell();
}

interface Car {
    void accept(IVisitor visitor);
}

class Engine implements Car {
    public void accept(IVisitor visitor) {
        visitor.visitEngine();
    }
}

class Whell implements Car {
    public void accept(IVisitor visitor) {
        visitor.visitWhell();
    }
}

class CarVisitor implements IVisitor {

    @Override
    public void visitEngine() {
        System.out.println("visitor to Engine");
    }

    @Override
    public void visitWhell() {
        System.out.println("visitor to Engine");
    }
}