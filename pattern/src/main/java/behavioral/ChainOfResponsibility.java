package behavioral;

public class ChainOfResponsibility {
    public static void main(String[] args) {
        ConsoleMessagePrinter consolePrinter = new ConsoleMessagePrinter();
        FileMessagePrinter filePrinter = new FileMessagePrinter();
        DbMessagePrinter dbPrinter = new DbMessagePrinter();
        consolePrinter.setNextMessagePrinter(filePrinter);
        filePrinter.setNextMessagePrinter(dbPrinter);
        consolePrinter.print("hello");
    }
}

abstract class MessagePrinter{
    MessagePrinter nextMessagePrinter;

    void setNextMessagePrinter (MessagePrinter messagePrinter) {
        nextMessagePrinter = messagePrinter;
    }

    void print(String message) {
        printMessage(message);
        if (nextMessagePrinter != null) {
            nextMessagePrinter.print(message);
        }
    }
    abstract void printMessage(String message);
}

class ConsoleMessagePrinter extends MessagePrinter {
    @Override
    void printMessage(String message) {
        System.out.println("print to console: " + message);
    }
}

class FileMessagePrinter extends MessagePrinter {
    @Override
    void printMessage(String message) {
        System.out.println("print to file: " + message);
    }
}

class DbMessagePrinter extends  MessagePrinter {
    @Override
    void printMessage(String message) {
        System.out.println("print to db: " + message);
    }
}
