package com.singleton.ru.singleton;

public class MySingleton {
    int count;
    private static MySingleton counter = new MySingleton();

    private MySingleton() {
    }

    public static MySingleton getInstance() {
        return counter;
    }

    public synchronized void incrementCount() {
        count++;
    }

    public int getCount() {
        return count;
    }

}
