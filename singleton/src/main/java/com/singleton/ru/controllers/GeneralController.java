package com.singleton.ru.controllers;

import com.singleton.ru.enumSingleton.MyEnumSingleton;
import com.singleton.ru.singleton.MySingleton;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@CrossOrigin(origins = "http://asdasdasd.com")
@RestController
@RequestMapping("/singleton")
public class GeneralController {

    @GetMapping("/enumAsyncInc100")
    public void enumAsyncInc100() {
        ExecutorService executorService = Executors.newFixedThreadPool(100);
        for (int i = 0; i < 100; i++) {
            executorService.submit(() -> {
                MyEnumSingleton counter = MyEnumSingleton.INSTANCE;
                counter.inc();
            });
        }
        executorService.shutdown();
    }

    @GetMapping("/getEnum")
    public String getEnumCount() {
        MyEnumSingleton counter = MyEnumSingleton.INSTANCE;
        return String.valueOf(counter.getCount());
    }

    @GetMapping("/inc")
    public void incCount() {
        MySingleton counter = MySingleton.getInstance();
        counter.incrementCount();
    }

    @GetMapping("/get")
    public String getCount() {
        MySingleton counter = MySingleton.getInstance();
        return String.valueOf(counter.getCount());
    }

    @GetMapping("/asyncInc100")
    public void asyncInc100() {
        ExecutorService executorService = Executors.newFixedThreadPool(100);
        for (int i = 0; i < 100; i++) {
            executorService.submit(() -> {
                MySingleton counter = MySingleton.getInstance();
                counter.incrementCount();
            });
        }
        executorService.shutdown();
    }

}
