package com.singleton.ru.enumSingleton;

public enum MyEnumSingleton {
    INSTANCE;

    int count;

    public synchronized void inc(){
        count++;
    }

    public int getCount(){
        return count;
    }
}
