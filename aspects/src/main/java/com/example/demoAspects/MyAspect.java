package com.example.demoAspects;



import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.stream.Collectors;

@Aspect
@Component
public class MyAspect {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

//    я запросил все public методы MyService с любым типом возврата * и количеством аргументов (..)
    @Pointcut("execution(public * com.example.demoAspects.MyService.*(..))")
    public void callAtMyServicePublic() { }

    @Before("callAtMyServicePublic()")
    public void beforeCallAtMethod1(JoinPoint jp) {
        String args = Arrays.stream(jp.getArgs())
                .map(a -> a.toString())
                .collect(Collectors.joining(","));
        logger.info("before " + jp.toString() + ", args=[" + args + "]");
    }

    @After("callAtMyServicePublic()")
    public void afterCallAt(JoinPoint jp) {
        logger.info("after " + jp.toString());
    }

//  Методы помеченные аннотацией AspectAnnotation - будут перехвачены.
    @Pointcut("@annotation(AspectAnnotation)")
    public void callAtMyServiceAnnotation() { }

    @Before("callAtMyServiceAnnotation()")
    public void beforeCallAt1(JoinPoint jp) {
        logger.info("Before " + jp.toString());
    }

    @After("callAtMyServiceAnnotation()")
    public void afterCallAt1(JoinPoint jp) {
        logger.info("After " + jp.toString());
    }

//  Методы помеченные аннотацией AspectAnnotationParamInMethod - будут перехвачены.
    @Pointcut("@annotation(AspectAnnotationParamInMethod) && args(count)")
    public void callAtMyServiceAnnotationParamMethod(int count) { }

    @Before("callAtMyServiceAnnotationParamMethod(count)")
    public void beforeCallAt2(JoinPoint jp, int count) {
        logger.info("Before " + jp.toString());
        System.out.println(count);
    }

    @After("callAtMyServiceAnnotationParamMethod(count)")
    public void afterCallAt12(JoinPoint jp, int count) {
        logger.info("After " + jp.toString());
        System.out.println(count);
    }
}
