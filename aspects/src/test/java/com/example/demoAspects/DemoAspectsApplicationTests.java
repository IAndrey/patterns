package com.example.demoAspects;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoAspectsApplicationTests {

    @Autowired
    private MyService service;

    @Test
    public void testLoggable() {
        List<String> list = new ArrayList();
        list.add("test");

        service.method1(list);
        service.method2();
        Assert.assertTrue(service.check());
    }

    @Test
    public void testLoggableAspectAnnotation() {
        service.method3();
        Assert.assertTrue(service.check());
    }

    @Test
    public void testLoggableAspectAnnotation_paramMethod() {
        service.method4(10);
        Assert.assertTrue(service.check());
    }
}
