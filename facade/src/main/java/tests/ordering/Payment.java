package tests.ordering;

import org.springframework.stereotype.Component;

@Component
public class Payment {

    public void signingOfWaybills() {
        System.out.println("Подписание накладных");
    }

    public void checkOfGoods() {
        System.out.println("Проверка товаров");
    }

    public void payment() {
        System.out.println("Оплата товаров");
    }
}
