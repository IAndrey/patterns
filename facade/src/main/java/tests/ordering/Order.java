package tests.ordering;

import org.springframework.stereotype.Component;

@Component
public class Order {

    public void productSearch() {
        System.out.println("Поиск товара на складе");
    }

    public void itemReservation() {
        System.out.println("Резервирование товара");
    }

    public void sendingItem() {
        System.out.println("Отправка товара покупателю");
    }

    public void arrivalItem() {
        System.out.println("Прибытие товара покупателю");
    }
}
