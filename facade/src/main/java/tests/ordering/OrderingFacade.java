package tests.ordering;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class OrderingFacade {

    @Autowired
    Order order;
    @Autowired
    Payment payment;

    public void order() {
        order.productSearch();
        order.itemReservation();
        order.sendingItem();
        order.arrivalItem();
    }

    public void payment() {
        payment.signingOfWaybills();
        payment.checkOfGoods();
        payment.payment();
    }
}
