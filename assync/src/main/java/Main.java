import sun.security.krb5.internal.TGSRep;

import java.util.Scanner;
import java.util.concurrent.*;

public class Main {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newSingleThreadExecutor();
        int x = 0;
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String string = scanner.next();
            if (string.equalsIgnoreCase("exit")) {
                break;
            }
            executor.submit(()-> {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(x + Integer.parseInt(string));
            });
        }
        executor.shutdown();
    }
}
