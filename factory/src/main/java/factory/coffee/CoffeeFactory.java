package factory.coffee;

import factory.Drink;
import factory.DrinkFactory;

public class CoffeeFactory extends DrinkFactory {

    @Override
    public Drink create() {
        return new Coffee();
    }
}
