package factory.coffee;

import factory.Drink;

public class Coffee implements Drink {

    @Override
    public String getType() {
        return "Coffee";
    }
}
