package factory;

public abstract class DrinkFactory {

    public abstract Drink create();
}
