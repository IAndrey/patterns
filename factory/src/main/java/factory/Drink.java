package factory;

public interface Drink {
    String getType();
}
