package factory.lemonade;

import factory.Drink;
import factory.DrinkFactory;

public class LemonadeFactory extends DrinkFactory {
    @Override
    public Drink create() {
        return new Lemonade();
    }
}
