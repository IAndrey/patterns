package factory.lemonade;

import factory.Drink;

public class Lemonade implements Drink {
    @Override
    public String getType() {
        return "Lemonade";
    }
}
