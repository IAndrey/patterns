import abstractFactory.factory.AbstractCarsFactory;
import abstractFactory.factory.CarsFactory;
import factory.coffee.CoffeeFactory;
import factory.DrinkFactory;
import factory.lemonade.LemonadeFactory;

public class Main {
    public static void main(String[] args) {
        startFactory();
    }

    public static void startAbstractFactory(){
        AbstractCarsFactory factory = new CarsFactory();
        System.out.println(factory.createBmw().getType());
        System.out.println(factory.createFord().getType());
    }

    public static void startFactory(){
        DrinkFactory drinkFactory1 = new CoffeeFactory();
        System.out.println(drinkFactory1.create().getType());
        DrinkFactory drinkFactory2 = new LemonadeFactory();
        System.out.println(drinkFactory2.create().getType());
    }
}
