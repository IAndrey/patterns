package abstractFactory.factory;

import abstractFactory.machine.BMW.BMW;
import abstractFactory.machine.Ford.Ford;

public abstract class AbstractCarsFactory {

    public abstract Ford createFord();

    public abstract BMW createBmw();
}
