package abstractFactory.factory;

import abstractFactory.machine.BMW.BmwCar;
import abstractFactory.machine.Ford.Ford;
import abstractFactory.machine.BMW.BMW;
import abstractFactory.machine.Ford.FordCar;

public class CarsFactory extends AbstractCarsFactory{


    @Override
    public Ford createFord() {
        return new FordCar();
    }

    @Override
    public BMW createBmw() {
        return new BmwCar();
    }
}
