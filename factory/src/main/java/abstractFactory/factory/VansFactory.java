package abstractFactory.factory;

import abstractFactory.machine.BMW.BmwVan;
import abstractFactory.machine.Ford.Ford;
import abstractFactory.machine.BMW.BMW;
import abstractFactory.machine.Ford.FordVan;

public class VansFactory extends AbstractCarsFactory{

    @Override
    public Ford createFord() {
        return new FordVan();
    }

    @Override
    public BMW createBmw() {
        return new BmwVan();
    }
}
