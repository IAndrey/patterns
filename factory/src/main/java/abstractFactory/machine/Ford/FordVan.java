package abstractFactory.machine.Ford;

public class FordVan implements Ford {
    @Override
    public String getType() {
        return "is Ford Van";
    }
}
