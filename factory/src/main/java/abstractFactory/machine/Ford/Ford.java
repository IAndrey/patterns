package abstractFactory.machine.Ford;

public interface Ford {
    public String getType();
}
