package abstractFactory.machine.Ford;

public class FordCar implements Ford {
    @Override
    public String getType() {
        return "is Ford Car";
    }
}
