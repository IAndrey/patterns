package abstractFactory.machine.BMW;

public interface BMW {
    public String getType();
}
