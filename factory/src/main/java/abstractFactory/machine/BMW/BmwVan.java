package abstractFactory.machine.BMW;

public class BmwVan implements BMW{
    @Override
    public String getType() {
        return "is BMW Van";
    }
}
