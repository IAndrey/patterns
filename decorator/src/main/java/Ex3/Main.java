package Ex3;

public class Main {
    public static void main(String[] args) {
        AccountingDepartment accountingDepartment = new AccountingDepartment(10000, "Вася");
        DecoratorAccountingDepartment decoratorAccountingDepartment = new DecoratorAccountingDepartment(accountingDepartment);
        accountingDepartment.withdrawCash(2000);
        decoratorAccountingDepartment.withdrawCash(2000);
        System.out.println(accountingDepartment.cash);
    }
}
