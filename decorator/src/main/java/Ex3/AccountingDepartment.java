package Ex3;

public class AccountingDepartment {
    int cash;
    String name;

    public AccountingDepartment(int cash, String name) {
        this.cash = cash;
        this.name = name;
    }

    public void depositCash(int cash) {
        this.cash += cash;
    }

    public void withdrawCash(int cash) {
        this.cash -= cash;
    }

    public int getCash() {
        return cash;
    }

    public String getName() {
        return name;
    }
}
