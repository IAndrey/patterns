package Ex3;

public class DecoratorAccountingDepartment {

    AccountingDepartment accountingDepartment;

    public DecoratorAccountingDepartment(AccountingDepartment accountingDepartment) {
        this.accountingDepartment = accountingDepartment;
    }

    public void withdrawCash(int cash) {
        accountingDepartment.cash -= cash;
        if (cash > 1000) {
            System.out.println("Сняли " + cash);
        }
    }

}
