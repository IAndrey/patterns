package primerIzKnigiPizza;

public interface Order {
    double getPrice();
    String getLabel();
}
