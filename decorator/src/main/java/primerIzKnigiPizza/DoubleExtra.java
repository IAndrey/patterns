package primerIzKnigiPizza;

//Декоратор чтобы избежать двукратной печати начинки на этикетке
public class DoubleExtra extends Extra {
    public DoubleExtra(Order order, String label, double price) {
        super(order, label, price);
    }

    @Override
    public double getPrice() {
        return (this.price * 2) + order.getPrice();
    }

    @Override
    public String getLabel() {
        return order.getLabel() + ". Double " + this.label;
    }
}
