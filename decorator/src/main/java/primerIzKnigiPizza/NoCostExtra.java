package primerIzKnigiPizza;

//Декоратор меняет строку label, но не добавляет ничего к цене пиццы.
public class NoCostExtra extends Extra {

    public NoCostExtra(Order order, String label, double price) {
        super(order, label, price);
    }

    @Override
    public double getPrice() {
        return order.getPrice();
    }
}
