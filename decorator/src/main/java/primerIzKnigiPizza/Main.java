package primerIzKnigiPizza;

public class Main {
    public static void main(String[] args) {
        Order fourSeasonPizza = new Pizza("Four Season Pizza", 10);
        fourSeasonPizza = new RegularExtra(fourSeasonPizza, "Peperoni", 4);
        fourSeasonPizza = new DoubleExtra(fourSeasonPizza, "Mozzarella", 2);
        fourSeasonPizza = new NoCostExtra(fourSeasonPizza, "Chili", 2);

        System.out.println(fourSeasonPizza.getPrice());
        System.out.println(fourSeasonPizza.getLabel());
    }
}
